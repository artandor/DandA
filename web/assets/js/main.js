$( document ).ready(function() {
  //Center the D&A logo to be by the side of the copyright text.
  $('footer.footer div .copyright img#footerLogoDA').css('margin-top', ($('footer.footer div .copyright').height() / 2) - 15)
});

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}