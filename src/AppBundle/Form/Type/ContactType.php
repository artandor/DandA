<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('name',
        TextType::class,
        [
          'label' => 'form.contact.name.label',
          'attr' => [
            'placeholder' => 'form.contact.name.placeholder',
          ],
          'required' => TRUE,
        ])
      ->add('object',
        TextType::class,
        [
          'label' => 'form.contact.object.label',
          'attr' => [
            'placeholder' => 'form.contact.object.placeholder',
          ],
          'required' => TRUE,
        ])
      ->add('mail',
        EmailType::class,
        [
          'label' => 'form.contact.mail.label',
          'attr' => [
            'placeholder' => 'form.contact.mail.placeholder',
          ],
          'required' => TRUE,
        ])
      ->add('message',
        TextareaType::class,
        [
          'label' => 'form.contact.message.label',
          'attr' => [
            'placeholder' => 'form.contact.message.placeholder',
          ],
          'required' => TRUE,
        ])
      ->add('submit',
        SubmitType::class,
        [
          'label' => 'form.contact.submit'
      ]
      );
  }

  public function configureOptions(OptionsResolver $resolver) {

  }

  public function getBlockPrefix() {
    return 'app_bundle_contact';
  }
}
