<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller {

  /**
   * @Route("/ajax/subscribeNewsletter", name="subscribeNewsletter")
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return string
   */
  public function subscribeNewsletterAction(Request $request) {
    if ($request->isXmlHttpRequest()) {
      try {
        $sendinblue = $this->get('sendinblue_api');
        $result = $sendinblue->get_account();
        if ($result['code'] !== 'success'){
          return new JsonResponse('An error occured contacting the API, please try again later.');
        }
        dump($request);
        $data = [
          'email' => $request->request->get('subscriberEmail'),
          /*'attributes' => [
            'NOM' => 'michel',
            'PRENOM' => 'Pinpon',
          ],*/
          'listid' => [4],
        ];
        $sendinblue->create_update_user($data);
        return new JsonResponse('You are now subscribed to the newsletter.');
      } catch (Exception $e) {
        return new JsonResponse('An error occured contacting the API, please try again later.');
      }
    }
  }
}
