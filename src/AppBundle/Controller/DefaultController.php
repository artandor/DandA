<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\ContactType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller {

  /**
   * @Route("/", name="homepage")
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction(Request $request) {
    // replace this example code with whatever you need
    return $this->render('@App/default/index.html.twig');
  }

  /**
   * @Route("/playground_axel", name="playgroundAxel")
   */
  public function playgroundAxelAction(Request $request) {
    // replace this example code with whatever you need
    return $this->render('@App/default/playgroundAxel.html.twig');
  }

  /**
   * @Route("/contact", name="contact")
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Swift_Mailer $mailer
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   * @throws \OutOfBoundsException
   */
  public function contactAction(Request $request, \Swift_Mailer $mailer) {
    $form = $this->createForm(ContactType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $message = (new \Swift_Message('Contact'))
        ->setFrom(['contact@DetA.com' => 'Contact D&A'])
        ->setTo('designetassocies@gmail.com')
        ->setBody(
          $this->renderView(
            '@App/mail/contact/contact.html.twig',
            [
              'name' => $form->get('name')->getData(),
              'object' => $form->get('object')->getData(),
              'mail' => $form->get('mail')->getData(),
              'message' => $form->get('message')->getData(),
            ]
          ),
          'text/html'
        );
      $mailer->send($message);

      $session = new Session();
      $session->getFlashBag()
        ->add('successSubmit', 'form.contact.success.message');

      return $this->redirectToRoute(
        'contact');
    }

    return $this->render('@App/default/contact.html.twig', ['formContact' => $form->createView()]);
  }
}
