<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase {

  public function testIndexAndTranslations() {
    $client = static::createClient();

    $crawler = $client->request('GET', '/en/');

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertContains('Home', $crawler->filter('title')->text());

    $crawler = $client->request('GET', '/fr/');

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertContains('Accueil', $crawler->filter('title')->text());

    #TODO add test translation for spannish
  }

  public function testAccessAdmin(){
    $client = static::createClient();

    $client->request('GET', '/admin');

    $this->assertEquals(302, $client->getResponse()->getStatusCode());
  }

  public function testConnectionApi(){
    $kernel = static::createKernel();
    $kernel->boot();

    $container = $kernel->getContainer();

    $sendinblue = $container->get('sendinblue_api');
    $result = $sendinblue->get_account();

    $this->assertEquals('success', $result['code']);
    $this->assertEquals('designetassocies@gmail.com', $result['data'][2]['email']);

  }
}
